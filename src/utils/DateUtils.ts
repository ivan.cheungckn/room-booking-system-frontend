export function getDateString(date: Date, type: string) {
	if (type === "full") {
		return (
			date.getFullYear().toString().padStart(2, "0") +
			date.getMonth().toString().padStart(2, "0") +
			date.getDate().toString().padStart(2, "0") +
			date.getHours().toString().padStart(2, "0") +
			date.getMinutes().toString().padStart(2, "0")
		);
	} else if (type === "date") {
		return (
			date.getFullYear().toString().padStart(2, "0") +
			date.getMonth().toString().padStart(2, "0") +
			date.getDate().toString().padStart(2, "0")
		);
	}
}
