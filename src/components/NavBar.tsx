import React, { useCallback } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import { IUser } from "../model/IUser";

interface IProps {
	user: IUser;
	setUser: (user: IUser) => void;
}

export const NavBar: React.FC<IProps> = (props) => {
	const logout = useCallback(async () => {
		const response = await fetch(
			`${process.env.REACT_APP_BACKEND_URL}/api/v1/users/logout`,
			{
				method: "POST",
				credentials: "include",
			}
		);
		const result = await response.json();
		if (result.message) alert(result.message);
		if (response.status === 200) {
			props.setUser({});
		}
	}, [props]);
	return (
		<Navbar bg="dark" variant="dark">
			<Container>
				<Navbar.Brand as={Link} to="/">
					Booking System
				</Navbar.Brand>
				<Nav>
					{!props.user || !props.user.id ? (
						<>
							<Nav.Link as={Link} eventKey={1} to="/register">
								Register
							</Nav.Link>
							<Nav.Link as={Link} eventKey={2} to="/login">
								Login
							</Nav.Link>
						</>
					) : (
						<>
							<Nav.Link as={Link} eventKey={1} to="/my-bookings">
								My Bookings
							</Nav.Link>
							<Nav.Link
								as={Link}
								eventKey={2}
								to="#"
								onClick={logout}
							>
								Logout
							</Nav.Link>
						</>
					)}
				</Nav>
			</Container>
		</Navbar>
	);
};
