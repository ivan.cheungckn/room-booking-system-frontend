import React, { useCallback, useEffect, useState } from "react";
import { IBooking } from "../model/IBookings";
import { IUser } from "../model/IUser";
import MyBookingTable from "./MyBookingTable";

interface IProps {
	user: IUser;
}
const MyBookingsPage: React.FC<IProps> = (props) => {
	const [bookings, setBookings] = useState([] as IBooking[]);

	const fetchMyBookings = useCallback(async () => {
		const response = await fetch(
			`${process.env.REACT_APP_BACKEND_URL}/api/v1/bookings/my`,
			{
				method: "GET",
				credentials: "include",
			}
		);
		const result = await response.json();
		if (response.status === 200) {
			for (const booking of result) {
				booking.startDate = new Date(booking.startDate);
				booking.endDate = new Date(booking.endDate);
			}
			setBookings(result);
		}
	}, []);

	useEffect(() => {
		fetchMyBookings();
	}, [fetchMyBookings]);
	return (
		<MyBookingTable fetchMyBookings={fetchMyBookings} bookings={bookings} />
	);
};

export default MyBookingsPage;
