import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

interface IRegisterInfo {
	username: string;
	phone: string;
	password: string;
	name: string;
}

export const RegisterPage: React.FC<any> = (props) => {
	const [name, setName] = useState("");
	const [username, setUsername] = useState("");
	const [phone, setPhone] = useState("");
	const [password, setPassword] = useState("");
	const navigate = useNavigate();
	function validateForm() {
		return (
			username.length > 0 &&
			phone.length > 0 &&
			name.length > 0 &&
			password.length > 0
		);
	}

	async function handleSubmit(event: any) {
		event.preventDefault();
		const registerInfo: IRegisterInfo = {
			name,
			username,
			password,
			phone,
		};
		const response = await fetch(
			`${process.env.REACT_APP_BACKEND_URL}/api/v1/users/register`,
			{
				method: "POST",
				body: JSON.stringify(registerInfo),
				credentials: "include",
				headers: {
					"Content-Type": "application/json",
				},
			}
		);
		// console.log(res);
		const result = await response.json();
		if (result.message) alert(result.message);
		if (response.status === 201) {
			alert("Successfully Created An Account");
			navigate("/login");
		}
		// console.log(response);
		// navigate("/");
	}

	return (
		<Form onSubmit={handleSubmit}>
			<Form.Group className="lg" controlId="username">
				<Form.Label>
					Username (Use your staff Id, start with C or P){" "}
				</Form.Label>
				<Form.Control
					autoFocus
					type="username"
					value={username}
					onChange={(e) => setUsername(e.target.value)}
				/>
			</Form.Group>

			<Form.Group className="lg" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="lg" controlId="name">
				<Form.Label>Name</Form.Label>
				<Form.Control
					type="name"
					value={name}
					onChange={(e) => setName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="lg" controlId="phone">
				<Form.Label>Phone</Form.Label>
				<Form.Control
					type="number"
					value={phone}
					onChange={(e) => setPhone(e.target.value)}
				/>
			</Form.Group>
			<Button className="lg" type="submit" disabled={!validateForm()}>
				Register
			</Button>
		</Form>
	);
};
