import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Button, Col, Row } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { IBooking } from "../model/IBookings";
import { IRoom } from "../model/IRoom";
import { IUser } from "../model/IUser";
import { getDateString } from "../utils/DateUtils";
import CreateBookingForm from "./CreateBookingForm";
import { ScheduleTable } from "./ScheduleTable";

interface IProps {
	user: IUser;
}
const SchedulePage: React.FC<IProps> = (props) => {
	const [rooms, setRooms] = useState([] as IRoom[]);
	const [bookings, setBookings] = useState([] as IBooking[]);
	const [showForm, setShowForm] = useState(false);
	const [timeSlots, setTimeSlots] = useState([] as string[]);
	const [selectedDate, setSelectedDate] = useState(new Date());
	const handleShowForm = () => setShowForm(true);

	function generateTimeSlotChoices() {
		const minute = "00";
		const timeSlotChoices = [];
		for (let i = 0; i < 24; i++) {
			let timeSlot = "";
			const hour = i < 10 ? `0${i}` : i;
			timeSlot += hour + minute;
			timeSlotChoices.push(timeSlot);
		}
		setTimeSlots(timeSlotChoices);
	}
	const getRooms = useCallback(async () => {
		const response = await fetch(
			`${process.env.REACT_APP_BACKEND_URL}/api/v1/rooms`,
			{
				method: "GET",
				credentials: "include",
			}
		);
		const result = await response.json();
		if (response.status === 200) {
			setRooms(result);
		}
	}, []);
	const fetchBookings = useCallback(
		async (startDate: Date, endDate: Date) => {
			const response = await fetch(
				`${
					process.env.REACT_APP_BACKEND_URL
				}/api/v1/bookings?startDate=${startDate.toISOString()}&endDate=${endDate.toISOString()}`,
				{
					method: "GET",
					credentials: "include",
				}
			);
			const result = await response.json();
			// if (result.message) alert(result.message);
			if (response.status === 200) {
				for (const booking of result) {
					booking.startDate = new Date(booking.startDate);
					booking.endDate = new Date(booking.endDate);
				}
				setBookings(result);
			}
		},
		[]
	);
	const scheduleMap = useMemo(() => {
		const scheduleMap = {} as any;
		for (const booking of bookings) {
			const hourDiff =
				(booking.endDate.getTime() - booking.startDate.getTime()) /
				3600000;
			for (let hour = 0; hour < hourDiff; hour++) {
				let startDate = new Date(booking.startDate.getTime());
				startDate.setTime(startDate.getTime() + hour * 60 * 60 * 1000);
				const startDateString =
					startDate.getHours().toString().padStart(2, "0") +
					startDate.getMinutes().toString().padStart(2, "0");
				if (scheduleMap[booking.room.code]) {
					scheduleMap[booking.room.code][startDateString] = booking;
				} else {
					scheduleMap[booking.room.code] = {};
					scheduleMap[booking.room.code][startDateString] = booking;
				}
			}
		}
		return scheduleMap;
	}, [bookings]);

	useEffect(() => {
		generateTimeSlotChoices();
		getRooms();
	}, [getRooms]);
	useEffect(() => {
		let startDate = new Date(selectedDate.getTime());
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);
		startDate.setMilliseconds(0);
		const one_day_in_milliseconds = 86400000;
		let endDate = new Date(startDate.getTime() + one_day_in_milliseconds);
		fetchBookings(startDate, endDate);
	}, [selectedDate, fetchBookings]);
	return (
		<>
			<Row>
				<Col className="d-flex justify-content-between">
					<DatePicker
						selected={selectedDate}
						onChange={(date) => (date ? setSelectedDate(date) : "")}
					/>
					{props.user.id ? (
						<>
							<Button variant="success" onClick={handleShowForm}>
								Create Booking
							</Button>

							<CreateBookingForm
								rooms={rooms}
								setFormShow={setShowForm}
								showForm={showForm}
								timeSlots={timeSlots}
								scheduleMap={scheduleMap}
								selectedDate={selectedDate}
								fetchBookings={fetchBookings}
							/>
						</>
					) : (
						""
					)}
				</Col>
			</Row>
			<ScheduleTable
				rooms={rooms}
				timeSlots={timeSlots}
				bookings={bookings}
				scheduleMap={scheduleMap}
				selectedDate={getDateString(selectedDate, "date") || ""}
			/>
		</>
	);
};

export default SchedulePage;
