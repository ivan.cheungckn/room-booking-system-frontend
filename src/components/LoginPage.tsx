import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { IUser } from "../model/IUser";

interface ILoginInfo {
	username: string;
	password: string;
}

interface IProps {
	setUser: (user: IUser) => void;
}

export const LoginPage: React.FC<IProps> = (props) => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const navigate = useNavigate();

	function validateForm() {
		return username.length > 0 && password.length > 0;
	}

	async function handleSubmit(event: any) {
		event.preventDefault();
		const loginInfo: ILoginInfo = {
			username,
			password,
		};
		const response = await fetch(
			`${process.env.REACT_APP_BACKEND_URL}/api/v1/users/login`,
			{
				method: "POST",
				body: JSON.stringify(loginInfo),
				credentials: "include",
				headers: {
					"Content-Type": "application/json",
				},
			}
		);
		console.log(response);
		const result = await response.json();
		console.log(result);
		if (result.message) alert(result.message);
		if (response.status === 200) {
			alert("Login Success");
			props.setUser(result);
			navigate("/");
		}
	}

	return (
		<Form onSubmit={handleSubmit}>
			<Form.Group className="lg" controlId="username">
				<Form.Label>Username</Form.Label>
				<Form.Control
					autoFocus
					type="username"
					value={username}
					onChange={(e) => setUsername(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="lg" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
				/>
			</Form.Group>
			<Button className="lg" type="submit" disabled={!validateForm()}>
				Login
			</Button>
		</Form>
	);
};
