import React, { useMemo } from "react";
import { Button, Table } from "react-bootstrap";
import { IBooking } from "../model/IBookings";

interface IProps {
	bookings: IBooking[];
	fetchMyBookings: () => void;
}
const MyBookingTable: React.FC<IProps> = (props) => {
	const columnsData = useMemo(() => {
		const cols = [
			{ header: "ID", accessor: "id" },
			{ header: "Room", accessor: "room" },
			{ header: "Start Date", accessor: "startDate" },
			{ header: "End Date", accessor: "endDate" },
			{ header: "Remove", accessor: "Remove" },
		];
		return cols;
	}, []);

	const currentDate = new Date();
	const removeBooking = async (bookingId: number) => {
		const response = await fetch(
			`${process.env.REACT_APP_BACKEND_URL}/api/v1/bookings/${bookingId}`,
			{
				method: "DELETE",
				credentials: "include",
			}
		);
		// console.log(res);
		const result = await response.json();
		if (result.message) alert(result.message);
		if (response.status === 200) {
			alert("Successfully Removed a Booking");
			props.fetchMyBookings();
		}
	};

	const handleRemoveBooking = async (event: any) => {
		if (window.confirm("Remove Booking?") === true) {
			console.log(
				"remove" + event.target.getAttribute("data-booking-id")
			);
			const bookingId = event.target.getAttribute("data-booking-id");
			await removeBooking(bookingId);
		}
	};
	return (
		<Table responsive hover bordered>
			<thead>
				<tr>
					{columnsData.map((column) => {
						return <th key={column.header}>{column.header}</th>;
					})}
				</tr>
			</thead>
			<tbody>
				{props.bookings.map((booking) => {
					return (
						<tr key={booking.id}>
							<td>{booking.id}</td>
							<td>{booking.room.code}</td>
							<td>{booking.startDate.toLocaleString()}</td>
							<td>{booking.endDate.toLocaleString()}</td>
							<td>
								{currentDate > booking.startDate ? (
									""
								) : (
									<Button
										variant="danger"
										data-booking-id={booking.id}
										onClick={handleRemoveBooking}
									>
										Remove Booking
									</Button>
								)}
							</td>
						</tr>
					);
				})}
			</tbody>
		</Table>
	);
};

export default MyBookingTable;
