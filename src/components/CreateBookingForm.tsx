import React from "react";
import { useState } from "react";
import { useCallback } from "react";
import { useMemo } from "react";
import { Button, Col, Row, Form, Modal } from "react-bootstrap";
import Select from "react-select";
import { IRoom } from "../model/IRoom";

interface IProps {
	showForm: boolean;
	timeSlots: string[];
	scheduleMap: any;
	setFormShow: (bool: boolean) => void;
	selectedDate: Date;
	fetchBookings: (startDate: Date, endDate: Date) => void;
	rooms: IRoom[];
}

interface IOptionRoom {
	value: string;
	label: string;
}

const CreateBookingForm: React.FC<IProps> = (props) => {
	const oneHourInMs = 3600000;
	const {
		showForm,
		timeSlots,
		scheduleMap,
		rooms,
		setFormShow,
		selectedDate,
		fetchBookings,
	} = props;

	const [selectedTimeSlots, setSelectedTimeSlots] = useState([]);
	const [selectedRoom, setSelectedRoom] = useState({
		value: "",
		label: "",
	} as IOptionRoom);

	const handleFormClose = useCallback(() => {
		setFormShow(false);
		setSelectedTimeSlots([]);
		setSelectedRoom({ value: "", label: "" });
	}, [setFormShow]);
	const handleOptionChange = useCallback((selectedOptions) => {
		setSelectedTimeSlots(selectedOptions);
	}, []);
	const handleRoomChange = useCallback((selectedOption) => {
		setSelectedRoom(selectedOption);
		setSelectedTimeSlots([]);
	}, []);
	const timeSlotOptions = useMemo(() => {
		const options = [];
		for (let i = 0; i < timeSlots.length; i++) {
			const selectedRoomValue: string = selectedRoom.value;
			if (selectedRoomValue === "") return;
			const timeSlot = timeSlots[i];
			if (
				scheduleMap[selectedRoomValue] &&
				scheduleMap[selectedRoomValue][timeSlot]
			) {
				continue;
			} else {
				const tillTimeSlot = timeSlots[i + 1]
					? timeSlots[i + 1]
					: timeSlots[0];
				options.push({
					value: timeSlot,
					label: `${timeSlot}-${tillTimeSlot}`,
				});
			}
		}
		return options;
	}, [selectedRoom, scheduleMap, timeSlots]);
	const handleCreateBooking = async () => {
		selectedTimeSlots.forEach(async (option: any) => {
			const startDate = new Date(selectedDate.getTime());
			startDate.setMilliseconds(0);
			startDate.setSeconds(0);
			startDate.setMinutes(0);
			startDate.setHours(option.value.substring(0, 2));
			const endDate = new Date(startDate.getTime() + oneHourInMs);

			await createBooking(
				selectedRoom.value,
				startDate.toISOString(),
				endDate.toISOString()
			);
		});
		handleFormClose();
	};
	const createBooking = useCallback(
		async (room: string, startDate: string, endDate: string) => {
			const body = { room, startDate, endDate };
			const response = await fetch(
				`${process.env.REACT_APP_BACKEND_URL}/api/v1/bookings`,
				{
					method: "POST",
					body: JSON.stringify(body),
					credentials: "include",
					headers: {
						"Content-Type": "application/json",
					},
				}
			);
			const result = await response.json();
			if (result.message) alert(result.message);
			if (response.status === 201) {
				alert("Successfully Created a Booking");
				let startDate = new Date(selectedDate.getTime());
				startDate.setHours(0);
				startDate.setMinutes(0);
				startDate.setSeconds(0);
				startDate.setMilliseconds(0);
				const one_day_in_milliseconds = 86400000;
				let endDate = new Date(
					startDate.getTime() + one_day_in_milliseconds
				);
				fetchBookings(startDate, endDate);
			}
		},
		[selectedDate, fetchBookings]
	);
	const roomOptions = useMemo(() => {
		return rooms.map((room: IRoom, i: number) => {
			return {
				value: room.code,
				label: room.code,
			};
		});
	}, [rooms]);
	return (
		<>
			<Modal
				show={showForm}
				onHide={handleFormClose}
				backdrop="static"
				keyboard={false}
			>
				<Modal.Header closeButton>
					<Modal.Title>Create Booking</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						<Form.Group as={Row} className="mb-3">
							<Form.Label column sm="2">
								Room
							</Form.Label>
							<Col sm="10">
								<Form.Control
									as={Select}
									placeholder="room"
									options={roomOptions}
									onChange={handleRoomChange}
								/>
							</Col>
						</Form.Group>
						<Form.Group as={Row} className="mb-3">
							<Form.Label column sm="2">
								TimeSlots
							</Form.Label>
							<Col sm="10">
								<Form.Control
									as={Select}
									isMulti={true}
									closeMenuOnSelect={false}
									placeholder="time"
									options={timeSlotOptions}
									onChange={handleOptionChange}
									value={selectedTimeSlots}
								/>
							</Col>
						</Form.Group>
					</Form>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleFormClose}>
						Close
					</Button>
					<Button variant="primary" onClick={handleCreateBooking}>
						Create Booking
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
};

export default CreateBookingForm;
