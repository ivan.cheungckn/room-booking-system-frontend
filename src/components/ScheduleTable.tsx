import { useMemo } from "react";
import { Table } from "react-bootstrap";
import { IBooking } from "../model/IBookings";
import { IRoom } from "../model/IRoom";

interface IProps {
	rooms: IRoom[];
	timeSlots: string[];
	bookings: IBooking[];
	scheduleMap: any;
	selectedDate: string;
}

export const ScheduleTable: React.FC<IProps> = (props) => {
	const rowsData = useMemo(() => {
		const rows = [];
		for (let i = 0; i < props.timeSlots.length; i++) {
			const tillTimeSlot = props.timeSlots[i + 1]
				? props.timeSlots[i + 1]
				: props.timeSlots[0];
			const row = [
				{
					timeSlot: props.timeSlots[i],
					name: `${props.timeSlots[i]}-${tillTimeSlot}`,
					room: "",
					booking: {} as IBooking,
				},
			];
			for (const room of props.rooms) {
				row.push({
					room: room.code,
					timeSlot: props.timeSlots[i],
					name: "",
					booking: props.scheduleMap[room.code]
						? props.scheduleMap[room.code][props.timeSlots[i]]
						: ({} as IBooking),
				});
			}
			rows.push(row);
		}
		return rows;
	}, [props.rooms, props.timeSlots, props.scheduleMap]);
	const columnsData = useMemo(() => {
		const cols = [{ header: "Time", accessor: "time" }];
		props.rooms.forEach((room) =>
			cols.push({ header: room.code, accessor: room.code })
		);
		return cols;
	}, [props.rooms]);
	return (
		<Table responsive hover bordered>
			<thead>
				<tr>
					{columnsData.map((column) => {
						return <th key={column.header}>{column.header}</th>;
					})}
				</tr>
			</thead>
			<tbody>
				{rowsData.map((row, i) => {
					return (
						<tr key={i}>
							{row.map((cell, i) => {
								return (
									<td
										key={i}
										data-time-slot={cell.timeSlot}
										data-room={cell.room}
										data-booking={cell.booking?.id}
										data-booking-name={
											cell.booking?.user?.name
										}
										data-booking-phone={
											cell.booking?.user?.phone
										}
									>
										{cell.name ? (
											cell.name
										) : (
											<>
												<div>
													{cell.booking?.user?.name}
												</div>{" "}
												<div>
													{cell.booking?.user?.phone}
												</div>
											</>
										)}
									</td>
								);
							})}
						</tr>
					);
				})}
			</tbody>
		</Table>
	);
};
