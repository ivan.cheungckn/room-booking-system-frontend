import React, { useCallback, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import { LoginPage } from "./components/LoginPage";
import MyBookingsPage from "./components/MyBookingsPage";
import { NavBar } from "./components/NavBar";
import { RegisterPage } from "./components/RegisterPage";
import SchedulePage from "./components/SchedulePage";
import { IUser } from "./model/IUser";

function App() {
	const [user, setUser] = useState({} as IUser);

	const fetchCurrentUser = useCallback(async () => {
		const response = await fetch(
			`${process.env.REACT_APP_BACKEND_URL}/api/v1/users/current`,
			{
				method: "GET",
				credentials: "include",
			}
		);
		const result = await response.json();
		if (response.status === 200) {
			setUser(result);
		}
	}, []);
	useEffect(() => {
		fetchCurrentUser();
	}, [fetchCurrentUser]);
	return (
		<Container>
			<NavBar user={user} setUser={setUser}></NavBar>
			<Routes>
				<Route path="" element={<SchedulePage user={user} />}></Route>
				<Route
					path="/my-bookings"
					element={<MyBookingsPage user={user} />}
				></Route>
				<Route path="register" element={<RegisterPage />}></Route>
				<Route
					path="login"
					element={<LoginPage setUser={setUser} />}
				></Route>
			</Routes>
		</Container>
	);
}

export default App;
