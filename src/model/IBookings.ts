import { IRoom } from "./IRoom";
import { IUser } from "./IUser";

export interface IBooking {
	id: number;
	room: IRoom;
	startDate: Date;
	endDate: Date;
	user?: IUser;
}
