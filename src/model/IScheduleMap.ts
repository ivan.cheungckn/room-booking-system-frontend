import { IBooking } from "./IBookings";

export interface IScheduleMap {
	code: ISchedule;
}

interface ISchedule {
	date: IBooking;
}
