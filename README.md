# Room Booking System

ivanbookingsystem.xyz

## backend

https://gitlab.com/ivan.cheungckn/room-booking-system-backend

## Register

For Cola and Pepsi's staffs, assume that they can register with their staff ID, eg. Cola's staff code start with `C` and Pepsi's staff code start with `P`

## Booking

They can create bookings but they cannot book two meeting room at the same time.

### tech

frontend: React

backend: Nestjs

deployed with gitlab CI/CD to AWS
